FROM debian:stable
MAINTAINER itzhak@behemoth.co.il

RUN apt update
RUN apt -y install certbot openssl openjdk-11-jre-headless

EXPOSE 80:80/tcp
ENTRYPOINT ["tail", "-f", "/dev/null"]
